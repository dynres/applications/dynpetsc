#ifndef DYN_PETSC 
#define DYN_PETSC

#include <petsc.h>

#define PETSC_ERROR_CANCELED -1

static const char world_pset_name[] = "mpi://WORLD";
static const char self_pset_name[] = "mpi://SELF";
static const char BOOL_STRING_TRUE[] = "True";
static const char main_pset_key[] = "main_pset";
static const char petsc_cancel[] = "petsc://cancel";
static const char petsc_cancel_ack[] = "petsc://cancel/ack";
static const PetscInt max_int_len = 10;

static MPI_Session session;
static MPI_Info info;
static MPI_Request psetop_req = MPI_REQUEST_NULL;
static char main_pset_name[MPI_MAX_PSET_NAME_LEN] = "";
static char add_pset_name[MPI_MAX_PSET_NAME_LEN] = "";
static char sub_pset_name[MPI_MAX_PSET_NAME_LEN] = "";
static bool is_dynamic;
static bool is_canceled;
static int psetop_op = MPI_PSETOP_NULL;

static MPI_Comm comm_world = MPI_COMM_NULL;
static PetscMPIInt world_rank = -1;
static PetscMPIInt world_size;

static int terminate = false;
static int petsc_psetop_pending = 0;

static PetscLogDouble t_start_global, t_start, t_end;
static PetscLogDouble t_step, t_psetop = .0, t_psetop_query = .0, t_psetop_publish = .0, t_psetop_lookup = .0, t_redis = .0;
static PetscLogDouble t_step_sum = .0, t_psetop_sum = .0, t_redis_sum = .0;

static PetscBool disable_monitor = PETSC_FALSE;
static PetscBool advanced_monitor = PETSC_FALSE;

static char procs_add[max_int_len] = "4";
static char procs_sub[max_int_len] = "4";

int psetop_noutput = 0;
char **psetop_output = NULL;

enum NextOpAlg {
    SWING,
    GROW_ONLY,
    SHRINK_ONLY,
    RANDOM
};

const char *next_op_alg_names[7] = { "SWING", "GROW_ONLY", "SHRINK_ONLY", "RANDOM", "NextOpAlg", "", NULL };

static NextOpAlg next_op_alg = SWING;
static PetscInt next_op_interval = 1;
static PetscInt iters_since_last_pset_op = 0;
static PetscMPIInt prev_op = MPI_PSETOP_SHRINK;
static PetscMPIInt next_op = MPI_PSETOP_NULL;
static PetscInt rand_seed = -1;

MPI_Info petsc_info = MPI_INFO_NULL;

PetscErrorCode dyn_petsc_set_info(MPI_Info info){
    if(petsc_info != MPI_INFO_NULL){
        MPI_Info_free(&petsc_info);
    }

    if(info == MPI_INFO_NULL){
        petsc_info = MPI_INFO_NULL;
    }else{
        MPI_Info_dup(info, &petsc_info);
    }

    return 0;
}

PetscErrorCode DecideNextOp() {
    PetscFunctionBeginUser;

    if (world_rank != 0) {
        PetscFunctionReturn(PETSC_SUCCESS);
    }

    ++iters_since_last_pset_op;

    if (iters_since_last_pset_op != next_op_interval) {
        next_op = MPI_PSETOP_NULL;
        PetscFunctionReturn(PETSC_SUCCESS);
    }

    switch (next_op_alg) {
        case SWING:
        next_op = (prev_op == MPI_PSETOP_GROW ? MPI_PSETOP_SHRINK : MPI_PSETOP_GROW);
        prev_op = next_op;
        break;

        case GROW_ONLY:
        next_op = MPI_PSETOP_GROW;
        break;

        case SHRINK_ONLY:
        next_op = MPI_PSETOP_SHRINK;
        break;

        case RANDOM:
        next_op = (random() % 2 ? MPI_PSETOP_SHRINK : MPI_PSETOP_GROW);
        prev_op = next_op;
        break;

        default:
        next_op = MPI_PSETOP_NULL;
    }

    iters_since_last_pset_op = 0;

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode FreeStringArray(char ***array, int size) {
    PetscFunctionBeginUser;
    if(0 == size){
        *array = NULL;
        PetscFunctionReturn(PETSC_SUCCESS);
    }
    for (int i = 0; i < size; i++) {
        free((*array)[i]);
    }
    free(*array);
    *array = NULL;

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode DynPetscFinalize(){
    char *keys[1] = {
        (char*) petsc_cancel_ack
    };
    char **input_psets, **output_psets;
    char cancel_value[MPI_MAX_PSET_NAME_LEN] ="";
    int flag, setop, noutput = 0;
    MPI_Info info;

    char sub_pset[MPI_MAX_PSET_NAME_LEN] = "";
    char cancel_pset[MPI_MAX_PSET_NAME_LEN] = ""; 
    char boolean_string[16] = "";

    if(MPI_INFO_NULL != petsc_info){
        PetscCallMPI(MPI_Info_free(&petsc_info));
    }

    /* If we have a pending setop, primary needs to cancel it and send result to all procs */
    strcpy(sub_pset, main_pset_name);
    if (petsc_psetop_pending){
        if(world_rank == 0){
            /* Send the setop cancelation - op will be MPI_PSETOP_NULL if setop already applied */
            setop = MPI_PSETOP_CANCEL;                                                                                     
            input_psets = (char **)malloc(1 * sizeof(char *));                                                              
            input_psets[0] = strdup(main_pset_name);                                                                             
            noutput = 0;
            PetscCallMPI(MPI_Session_dyn_v2a_psetop(session, &setop, input_psets, 1, &output_psets, &noutput, MPI_INFO_NULL));       
            FreeStringArray(&input_psets, 1);
            FreeStringArray(&output_psets, noutput);
            /* If cancelation did not succeed finalize outstanding setop and store pset names */
            if (MPI_PSETOP_NULL == setop)                                                                                  
            {
                PetscCallMPI(MPI_Wait(&psetop_req, MPI_STATUS_IGNORE));
                if (MPI_PSETOP_NULL != psetop_op)                                                                            
                {                                                                                                   
                    strcpy(cancel_pset, psetop_output[1]);
                    strcpy(sub_pset, psetop_output[2]);                                                                 
                    PetscCallMPI(MPI_Session_dyn_finalize_psetop(session, (char *) main_pset_name));          
                    FreeStringArray(&psetop_output, psetop_noutput);                                            
                }                                                                       
            }
        }     
    }

    if(!terminate){
        PetscCallMPI(MPI_Bcast(sub_pset, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, comm_world));
        /* Check if this process is included in the final sub PSet */                                        
        PetscCallMPI(MPI_Session_get_pset_info(session, sub_pset, &info));                                                                            
        PetscCallMPI(MPI_Info_get(info, "mpi_included", 6, boolean_string, &flag));                                                                      
        PetscCallMPI(MPI_Info_free(&info));
    }  

    PetscCallMPI(MPI_Comm_disconnect(&comm_world));
    PETSC_COMM_WORLD = PETSC_COMM_SELF;

    if(world_rank == 0){
        /* Send a SUB operation to delete this PSet and inform spawned processes about cancelation */
        setop = MPI_PSETOP_SUB;
        input_psets = (char **)malloc(1 * sizeof(char *));                                                              
        input_psets[0] = strdup(sub_pset);
        noutput = 0;
        PetscCallMPI(MPI_Info_create(&info));
        PetscCallMPI(MPI_Info_set(info, "model", "DefaultSubModel()"));
        PetscCallMPI(MPI_Info_set(info, "output_space_generator", "output_space_generator_sub"));
        PetscCallMPI(MPI_Info_set(info, "input_pset_models_0", "NullPSetModel()"));   

        PetscCallMPI(MPI_Session_dyn_v2a_psetop(session, &setop, input_psets, 1, &output_psets, &noutput, info));
        PetscCallMPI(MPI_Info_free(&info));
        FreeStringArray(&input_psets, 1);
        FreeStringArray(&output_psets, noutput);

         /* If we have a cancel PSet, inform spawned processes about cancelation */
        if(0 != strcmp(cancel_pset, "")){
            sprintf(cancel_value, "%s/%s", petsc_cancel, sub_pset);
            /* Inform spawned processes about cancelation */
            PetscCallMPI(MPI_Info_create(&info));                                                                             
            PetscCallMPI(MPI_Info_set(info, main_pset_key, cancel_value));                                                  
            PetscCallMPI(MPI_Session_set_pset_data(session, cancel_pset, info));
            PetscCallMPI(MPI_Info_free(&info));
        }       
    }

    /* If we are part of the final sub operation Wait for primary proc */
    /* This potentially synchronizes with spawned but canceled procs in InitPetsc*/
    if(0 == strcmp(boolean_string, "True")){ 
        input_psets = (char **) malloc(sizeof(char *));
        input_psets[0] = strdup(sub_pset);
        PetscCallMPI(MPI_Session_pset_barrier(session, input_psets, 1, MPI_INFO_NULL));
        FreeStringArray(&input_psets, 1);
    }
    PetscCall(PetscFinalize());
    PetscFunctionReturn(PETSC_SUCCESS);
}



#define DEBUG_LOOP \
    int i = 0; \
    while (i == 0) \
        sleep(1);

PetscErrorCode RedistributeObjects();
PetscErrorCode CleanUp();
PetscErrorCode Step();
PetscErrorCode Monitor();

PetscErrorCode SetCommFromSession() {
    MPI_Group group;
    MPI_Info pset_info;
    char pset_alias[MPI_MAX_PSET_NAME_LEN];
    int flag;

    PetscFunctionBeginUser;

    /* Get the PSet alias to ensure we have a unique tag for communicator creation */
    MPI_Session_get_pset_info (session, main_pset_name, &pset_info);
    MPI_Info_get(pset_info, "mpi_alias", MPI_MAX_PSET_NAME_LEN, pset_alias, &flag);
    if(!flag){
        PetscFunctionReturn(PETSC_ERR_MPI);
    }

    PetscCallMPI(MPI_Group_from_session_pset(session, main_pset_name, &group));
    PetscCallMPI(MPI_Comm_create_from_group(group, pset_alias, MPI_INFO_NULL, MPI_ERRORS_ARE_FATAL, &comm_world));

    PetscCallMPI(MPI_Comm_rank(comm_world, &world_rank));
    PetscCallMPI(MPI_Comm_size(comm_world, &world_size));

    PetscCallMPI(MPI_Group_free(&group));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode GetInfoFromSession() {
    const int nkey = 1;
    char boolean_string[64];
    char **barrier_psets;
    char *keys[nkey] = {
        (char*) main_pset_key
    };
    PetscLogDouble start, end;
    PetscFunctionBeginUser;

    int key_exists, flag;

    PetscCall(PetscTime(&start));

    PetscCallMPI(MPI_Session_get_pset_data(session, (char*) world_pset_name, (char*) world_pset_name, keys, nkey, true, &info));

    PetscCall(PetscTime(&end));
    t_psetop_lookup = end - start;

    PetscCallMPI(MPI_Info_get(info, main_pset_key, MPI_MAX_PSET_NAME_LEN, main_pset_name, &key_exists));
    PetscCallMPI(MPI_Info_free(&info));

    if(0 == strncmp(main_pset_name, petsc_cancel, strlen(petsc_cancel))){
        barrier_psets = (char **) malloc(sizeof(char *));
        barrier_psets[0] = strdup(main_pset_name + strlen(petsc_cancel) + 1);
        printf("Canceled Procs barrier on PSet %s\n", barrier_psets[0]);
        fflush(NULL);
        MPI_Session_pset_barrier(session, barrier_psets, 1, MPI_INFO_NULL);
        FreeStringArray(&barrier_psets, 1);

        PetscCall(PetscFinalize());
        session = MPI_SESSION_NULL;
        return PETSC_ERROR_CANCELED;
    }

    if (!key_exists) {
        //printf("main pset key does not exist on pset info\n");
        PetscCallMPI(MPI_Abort(comm_world, PETSC_ERR_RETURN));
    }


    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitPetsc(int *argc, char ***argv) {
    PetscFunctionBeginUser;

    PetscCall(PetscInitializeSession(NULL, argc, argv, NULL, NULL));
    session    = PETSC_SESSION;
    is_dynamic = PetscIsDynamic;

    if (is_dynamic) {
        PetscCall(GetInfoFromSession());
    } else {
        // initially main pset is world
        strcpy(main_pset_name, world_pset_name);
    }

    if(MPI_SESSION_NULL == session){
        is_canceled = true;
        //PetscCall(DynPetscFinalize());
        PetscFunctionReturn(PETSC_ERROR_CANCELED);
    }else{
        is_canceled = false;
    }

    PetscCall(SetCommFromSession());
    PETSC_COMM_WORLD = comm_world;

    PetscCall(PetscOptionsHasName(NULL, NULL, "-disable_monitor", &disable_monitor));
    PetscCall(PetscOptionsHasName(NULL, NULL, "-advanced_monitor", &advanced_monitor));

    PetscCall(PetscOptionsGetString(NULL, NULL, "-procs_add", procs_add, max_int_len, NULL));
    PetscCall(PetscOptionsGetString(NULL, NULL, "-procs_sub", procs_sub, max_int_len, NULL));
    
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-next_op_interval", &next_op_interval, NULL));
    PetscCall(PetscOptionsGetEnum(NULL, NULL, "-next_op_alg", next_op_alg_names, (PetscEnum *) &next_op_alg, NULL));
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-random_seed", &rand_seed, NULL));

    if (rand_seed > 0) srand(rand_seed);

    if (is_dynamic && advanced_monitor) {

        PetscCallMPI(MPI_Reduce(&t_psetop_lookup, NULL, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SendPsetOp_private() {
    PetscLogDouble start, end;
    PetscFunctionBeginUser;

    char old_main_pset[MPI_MAX_PSET_NAME_LEN];
    MPI_Status status;

    int procs_sub_int = atoi(procs_sub);
    next_op = MPI_PSETOP_REPLACE;
    // primary process tries to send pset op
    if(!petsc_psetop_pending){
        if (world_rank == 0) {
            psetop_op = next_op;

            if(MPI_INFO_NULL == petsc_info){
                PetscCallMPI(MPI_Info_create(&petsc_info));

                if (psetop_op == MPI_PSETOP_SHRINK) {
                    PetscCallMPI(MPI_Info_set(petsc_info, "mpi_num_procs_sub", procs_sub));
                } else {
                    PetscCallMPI(MPI_Info_set(petsc_info, "mpi_num_procs_add", procs_sub));
                }
            }

            const int ninput = 1;
            char *input_psets[ninput] = {
                (char*) main_pset_name
            };

            //char **out_psets;
            //int nout = 0;
            psetop_noutput = 0;
            // send operation request
            PetscCallMPI(MPI_Session_dyn_v2a_psetop_nb(session, &psetop_op, input_psets, ninput, &psetop_output, &psetop_noutput, petsc_info, &psetop_req));
            //PetscCallMPI(MPI_Info_free(&petsc_info));
        }
        petsc_psetop_pending = 1;
    }

    // Query information from all processes and adjust main pset and PETSc resources accordingly
    if (advanced_monitor) PetscCall(PetscTime(&start));
    
    int flag = 0;
    if(world_rank == 0 && psetop_req != MPI_REQUEST_NULL){
        MPI_Test(&psetop_req, &flag, &status);
    }
    MPI_Bcast(&flag, 1, MPI_INT, 0, comm_world);
    if(flag){
        strcpy(old_main_pset, main_pset_name);

        if(world_rank == 0){
            strcpy(main_pset_name, psetop_output[2]);
            strcpy(add_pset_name, psetop_output[1]);
            strcpy(sub_pset_name, psetop_output[0]);
        }
        MPI_Bcast(main_pset_name, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, comm_world);
        MPI_Bcast(add_pset_name,  MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, comm_world);
        MPI_Bcast(sub_pset_name,  MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, comm_world);
    }
    //PetscCallMPI(MPI_Session_dyn_v2a_query_psetop(session, main_pset_name, main_pset_name, &op, &out_psets, &nout));
    if (advanced_monitor) {
        PetscCall(PetscTime(&end));
        PetscLogDouble elapsed = end - start;
        PetscCallMPI(MPI_Reduce(&elapsed, &t_psetop_query, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    if (!flag || 0 == strcmp(main_pset_name, old_main_pset)) {
        // PetscCall(PetscPrintf(comm_world, "Requested resource was not available. Will continue loop...\n"));
        PetscFunctionReturn(PETSC_ERR_RETURN);
    }

    petsc_psetop_pending = 0;

    if (world_rank == 0) {

        if(0 != strcmp(add_pset_name, "")){
            /* Publish the name of the new main PSet on the delta Pset */
            PetscCallMPI(MPI_Info_create(&info));
            // first out
            PetscCallMPI(MPI_Info_set(info, main_pset_key, main_pset_name));

            if (advanced_monitor) PetscCall(PetscTime(&start));
            PetscCallMPI(MPI_Session_set_pset_data(session, add_pset_name, info));
            PetscCallMPI(MPI_Info_free(&info));
            if (advanced_monitor) {
                PetscCall(PetscTime(&end));
                t_psetop_publish = end - start;
            }
        }
        
        psetop_req = MPI_REQUEST_NULL;
        PetscCall(FreeStringArray(&psetop_output, psetop_noutput));
    }

    // check if process is included in delta pset of shrink, if not terminate it
    int key_exists;
    char bool_str[6];
    PetscCallMPI(MPI_Session_get_pset_info(session, main_pset_name, &info));
    PetscCallMPI(MPI_Info_get(info, "mpi_included", 6, bool_str, &key_exists));
    PetscCallMPI(MPI_Info_free(&info));
    if (!key_exists || 0 != strcmp(bool_str, BOOL_STRING_TRUE)) {
        terminate = true;
    }
    

    // primary process finalizes psetop
    if (world_rank == 0) {
        PetscCallMPI(MPI_Session_dyn_finalize_psetop(session, old_main_pset));
    }

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SendPsetOp() {
    PetscFunctionBeginUser;

    PetscCall(DecideNextOp());

    if (world_rank == 0) PetscCall(PetscTime(&t_start));

    int pset_op_fail = SendPsetOp_private();

    if (world_rank == 0) {
        PetscCall(PetscTime(&t_end));
        t_psetop = t_end - t_start;
        t_psetop_sum += t_psetop;
    }

    PetscFunctionReturn(pset_op_fail);
}

PetscErrorCode AdvancedMonitor() {
    PetscFunctionBeginUser;

    if (!advanced_monitor || disable_monitor) PetscFunctionReturn(PETSC_SUCCESS);

    /*
    t_psetop_lookup = 0.0; // only new processes should send it
    PetscLogDouble max_lookup = .0;
    if (next_op == MPI_PSETOP_GROW) {
        PetscCallMPI(MPI_Reduce(&t_psetop_lookup, &max_lookup, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    const char *op = (next_op == MPI_PSETOP_GROW ? "GROW" : "SHRINK");
    PetscCall(PetscPrintf(comm_world, ", t_redis: %8.5f, t_psetop: %8.5f, t_psetop_query: %8.5f, t_psetop_publish: %8.5f, t_psetop_lookup: %8.5f, op: %6s\n",
                            t_redis, t_psetop, t_psetop_query, t_psetop_publish, max_lookup, op));
    */

    PetscFunctionReturn(PETSC_SUCCESS);
}


PetscErrorCode PrintResults() {
    PetscFunctionBeginUser;

    // Calculate elapsed time for each process
    PetscCall(PetscTime(&t_end));
    PetscLogDouble t_total = (t_end - t_start_global);

    PetscCall(PetscPrintf(comm_world, "\n\n"));
    PetscCall(PetscPrintf(comm_world, "Total time             %8.5f sec\n\n", t_total));

    PetscCall(PetscPrintf(comm_world, "Sum of steps           %8.5f sec %5.1f%%\n", t_step_sum, t_step_sum / t_total * 100.0));

    PetscCall(PetscPrintf(comm_world, "Comm overhead total    %8.5f sec %5.1f%%\n\n", t_total - t_step_sum, (1.0 - t_step_sum / t_total) * 100.0));

    PetscCall(PetscPrintf(comm_world, "Sum of psetop          %8.5f sec %5.1f%%\n", t_psetop_sum, t_psetop_sum / t_total * 100.0));
    PetscCall(PetscPrintf(comm_world, "Sum of redis           %8.5f sec %5.1f%%\n\n", t_redis_sum, t_redis_sum / t_total * 100.0));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode PrintNewLine() {
    PetscFunctionBeginUser;

    if (!disable_monitor && advanced_monitor) PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\n"));

    PetscFunctionReturn(PETSC_SUCCESS);
}



#endif